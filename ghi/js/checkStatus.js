const payloadCookie = await cookieStore.get("jwt_access_payload");
console.log('payloadcookiepay:', payloadCookie)
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(payloadCookie.value);
  console.log("decodedPayload:", decodedPayload);
  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);
  console.log("payload:", payload);

  // Print the payload
//   console.log(payload);

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  let perms = payload.user.perms;
  const loginlocation = document.getElementById("logged-in")
  const loginconference = document.getElementById("logged-in-conference")
  if (perms.includes("events.add_conference")){
    loginlocation.classList.remove("d-none");
    loginconference.classList.remove("d-none");
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (!(perms.includes("events.add_location"))){
    loginlocation.classList.add("d-none");
    loginconference.classList.add("d-none");
  }

}
