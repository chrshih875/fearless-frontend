import React, { useState, useEffect } from 'react';
import { json } from 'react-router-dom';

const ConferenceForm = (props) => {

    const [ name, setName ] = useState("");
    const [ starts, setStart ] = useState("");
    const [ ends, setEnd ] = useState("");
    const [description, setDescription ] = useState("");
    const [ maxp, setMaxp ] = useState("");
    const [ maxa, setMaxa ] = useState("");
    const [ location, setLocation ] = useState();
    const [ locations, fetchState ] = useState([]);

    const handleSubmit = async (submit) => {
      submit.preventDefault()
      const conferenceURL = "http://localhost:8000/api/conferences/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify({name: name,
          starts: starts,
          ends: ends,
          max_presentations: maxp,
          max_attendees: maxa,
          location: location
        }),
        headers: {
          "Content-type" : "application/json",
        },
      }
      const response = await fetch(conferenceURL, fetchConfig);
      if (response.ok){
        const newConference = await response.json();
        console.log(newConference)
      }
    }

    useEffect(() => {
        const getLocation = async () => {
            const response = await fetch('http://localhost:8000/api/locations/');
            const data = await response.json();
            fetchState(data.locations)
            console.log(data)
        }

        getLocation()
      }, [])

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" value={name} onChange={(event) => setName(event.target.value)} id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Starts" required type="date" name="starts" value={starts} onChange={(event) => setStart(event.target.value)} id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Ends" required type="date" name="ends" value={ends} onChange={(event) => setEnd(event.target.value)} id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Description" required type="text" name="description" value={description} onChange={(event) => setDescription(event.target.value)} id="description" className="form-control"/>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum presentations" required type="number" name="max_presentations" value={maxp} onChange={(event) => setMaxp(event.target.value)} id="max_presentations" className="form-control"/>
                <label htmlFor="maximum_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum attendees" required type="number" name="max_attendees" value={maxa} onChange={(event) => setMaxa(event.target.value)} id="max_attendees" className="form-control"/>
                <label htmlFor="maximum_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={(event => setLocation(event.target.value))} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map( location => {
                                    return <option key={location.id} value={location.id}>{location.name}</option>
                                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
