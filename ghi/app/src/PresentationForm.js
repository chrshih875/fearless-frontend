import React, { useState, useEffect } from 'react';

const PresentationForm = (props) => {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         {
    //             presenter_name: "",
    //             presenter_email: email,
    //             company_name: cname,
    //             title: title,
    //             synopsis: synopsis,
    //         }
    //     }
    // }

    const [ name, setName ] = useState("");
    const [ email, setEmail ] = useState("");
    const [ cname, setCname ] = useState("");
    const [ title, setTitle ] = useState("");
    const [ synopsis, setSynopsis ] = useState("");
    const [ conference, setConference ] = useState("");
    const [ conferences, setConferences ] = useState([]);

    const clearState = () => {
        setName("");
        setEmail("");
        setCname("");
        setTitle("");
        setSynopsis("");
        setConference("");
    }

    useEffect(() => {
        const getConference = async () => {
            const response = await fetch ("http://localhost:8000/api/conferences/")
            const data = await response.json()
            setConferences(data.conferences)
            console.log(data)
        }
        getConference()
    }, [])

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const formTag = document.getElementById('create-presentation-form');
        const formData = new FormData(formTag);
        console.log("FormData",formData)
        const confId = formData.get('conference')
        const presentationURL = `http://localhost:8000/api/conferences/${confId}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify({
            presenter_name: name,
            presenter_email: email,
            company_name: cname,
            title: title,
            synopsis: synopsis,
          }),
          headers: {
            "Content-type" : "application/json",
          },
        }
        const response = await fetch(presentationURL, fetchConfig);
        if (response.ok){
            const newConference = await response.json();
            console.log(newConference)
            PresentationForm().then(clearState)
        }
    }

    console.log("conferences ", conferences)
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form id="create-presentation-form" onSubmit={handleSubmit}>
                  <div className="form-floating mb-3">
                    <input value={name} onChange={(event) => setName(event.target.value)} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                    <label htmlFor="presenter_name">Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={email} onChange={(event) => setEmail(event.target.value)} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={cname} onChange={(event) => setCname(event.target.value)} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={title} onChange={(event) => setTitle(event.target.value)} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea value={synopsis} onChange={(event) => setSynopsis(event.target.value)} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                  </div>
                  <div className="mb-3">
                    <select  value={conference} onChange={(event) => setConference(event.target.value)} required name="conference" id="conference" className="form-select">
                      <option value="">Choose a conference</option>
                      {conferences.map( conference =>
                                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )}
                    </select>
                  </div>
                  <button onClick={clearState} className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
}

export default PresentationForm;

// import React from 'react';

// class PresentationForm extends React.Component{
//     constructor(props) {
//         super(props);
//         this.state = {
//             presenterName: '',
//             presenterEmail: '',
//             companyName: '',
//             title: '',
//             synopsis: '',
//             conferences: []
//         }
//         this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
//         this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
//         this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
//         this.handleTitleChange = this.handleTitleChange.bind(this);
//         this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
//         this.handleChangeConference = this.handleChangeConference.bind(this);

//     }
//     handlePresenterNameChange(event) {
//         const value = event.target.value;
//         this.setState({presenterName:value})
//     }
//     handlePresenterEmailChange(event) {
//         const value = event.target.value;
//         this.setState({presenterEmail:value})
//     }
//     handleCompanyNameChange(event) {
//         const value = event.target.value;
//         this.setState({companyName:value})
//     }
//     handleTitleChange(event) {
//         const value = event.target.value;
//         this.setState({title:value})
//     }
//     handleSynopsisChange(event) {
//         const value = event.target.value;
//         this.setState({synopsis:value})
//     }
//     handleChangeConference(event) {
//         const value = event.target.value;
//         this.setState({conference:value})
//     }

//     async handleSubmit(event){
//         // event.preventDefault();
//         // const data= {...this.state};
//         // data.presenter_name = data.presenterName;
//         // data.presenter_email = data.presenterEmail;
//         // data.company_name = data.companyName;
//         // delete data.presenterName;
//         // delete data.presenterEmail;
//         // delete data.companyName;
//         // delete data.conferences;
//         // console.log(data)

//         // const selectTag = document.getElementById('conference');
//         // const conferenceId = selectTag.options[selectTag.selectedIndex].value;
//         // const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
//         // const fetchConfig = {
//         // method: "post",
//         // body: JSON.stringify(data),
//         // headers: {
//         //     'Content-Type': './application/json',
//         // },
//         // };
//         // const response = await fetch(locationUrl, fetchConfig);
//         // if (response.ok) {
//         //     const newPresentation = await response.json();
//         //     console.log(newPresentation)
//         //     const cleared = {
//         //         presenterName: '',
//         //         presenterEmail: '',
//         //         companyName: '',
//         //         title: '',
//         //         synopsis: '',
//         //         conference: '',

//         //     };
//         //     this.setState(cleared);
//         // }
//     }



//     async componentDidMount() {
//         const url = 'http://localhost:8000/api/conferences/';
//         const response = await fetch(url);
//         if (response.ok) {
//           const data = await response.json();
//           this.setState({ conferences: data.conferences });
//           console.log("CDATA:::", data)
//         }
//       }





//     render(){
//         return(
//             <div className="container">
//             <div className="row">
//             <div className="offset-3 col-6">
//                 <div className="shadow p-4 mt-4">
//                 <h1>Create a new presentation</h1>
//                 <form id="create-presentation-form" onSubmit={this.handleSubmit} >
//                     <div className="form-floating mb-3">
//                     <input onChange={this.handlePresenterNameChange} value={this.state.presenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"></input>
//                     <label htmlFor="presenter_name">Presenter name</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                     <input onChange={this.handlePresenterEmailChange} value={this.state.presenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"></input>
//                     <label htmlFor="presenter_email">Presenter email</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                     <input onChange={this.handleCompanyNameChange} value={this.state.companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"></input>
//                     <label htmlFor="company_name">Company name</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                     <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control"></input>
//                     <label htmlFor="title">Title</label>
//                     </div>
//                     <div className="mb-3">
//                     <label htmlFor="synopsis">Synopsis</label>
//                     <textarea onChange={this.handleSynopsisChange} value={this.state.synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
//                     </div>
//                     <div className="mb-3">
//                     <select onChange={this.handleChangeConference} name="conference" id="conference" className="form-select" required>
//                       <option>Choose a conference</option>
//                       {this.state.conferences.map(conference => {
//                         return (
//                           <option key={conference.href} value={conference.href}>{conference.name}</option>
//                         )
//                       })}
//                     </select>
//                     </div>
//                     <button className="btn btn-primary">Create</button>
//                 </form>
//                 </div>
//             </div>
//             </div>
//             </div>
//         );
//     }
// }
// export default PresentationForm;
